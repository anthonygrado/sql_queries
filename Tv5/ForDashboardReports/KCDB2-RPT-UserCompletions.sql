DECLARE @UserFirstName nvarchar(50);
DECLARE @UserLastName nvarchar(50);
DECLARE @SearchFirstName nvarchar(50);
DECLARE @SearchLastName nvarchar(50);
DECLARE @SearchOrganization nvarchar(50);

--SET @UserFirstName = 'EMMYLOU'
SET @UserLastName = 'Grado'
SET @SearchOrganization = 'C84D529C-A47E-4C39-9433-A08B32464119'
SET @SearchFirstName = '%'+@UserFirstName+'%'
SET @SearchLastName = '%'+@UserLastName+'%'


/****** Script for SelectTopNRows command from SSMS  ******/
SELECT DISTINCT ORG.[OrganizationName] AS [User Organization]
	,L.[Learner_ID] AS [Learner LMS ID]
	,LTRIM(RTRIM(L.[FirstName])) AS [First Name]
	,LTRIM(RTRIM(L.[LastName])) AS [Last Name]
	,LSI.[LearnerSourceIdentity_ID] AS [LSI ID]
	,LSI.[SourceIdentity] AS [Learner LMS ID]
	,X.[Product Title]
	,X.[Completed Date]
	,X.[score]
	FROM [Learning_Products].[dbo].[Learner] L
	INNER JOIN [Learning_Products].[dbo].[Organizations] ORG
		ON L.[Organization_ID] = ORG.[Organization_ID]
	INNER JOIN [Learning_Products].[dbo].[LearnerSourceIdentity] LSI
		ON L.[Learner_ID] = LSI.[Learner_ID]
	INNER JOIN (SELECT QT.[id]
	,CONVERT(varchar(16),QT.[lastUpdatedUTC],120) AS [Completed Date]
	,QT.[questionnaireId]
	,QT.[organizationId]
	,QT.[usernameId]
	,CONCAT(PRO.[Title],' v',PRO.[VersionNumber],'.0') AS [Product Title]
	,QT.[productId]
	,QT.[attempt]
	,QT.[score]
	,QT.[requiredScore]
	,CONCAT(QT.[organizationId],'_',QT.[usernameId]) AS [QTID]
	FROM [Learning_Products].[dbo].[QuestionnaireTransactions] QT
	INNER JOIN [Learning_Products].[dbo].[Products] PRO
		ON QT.[productId] = PRO.[ItemID]
	LEFT JOIN [Learning_Products].[dbo].[CertificateTransactions] CT
		ON CONCAT(QT.[organizationId],QT.[usernameId]) = CONCAT(CT.[organizationId],CT.[usernameId])
	WHERE QT.[questionnaireId] NOT LIKE 'c%'
		AND QT.[score] >= QT.[requiredScore]) X
	ON CONCAT(ORG.[PMTID],'_',LSI.[SourceIdentity]) = X.[QTID]
	WHERE (L.[FirstName] LIKE @SearchFirstName
	OR L.[LastName] LIKE @SearchLastName)
	AND ORG.[PMTID] IN (''+@SearchOrganization+'')
	ORDER BY [User Organization], [Last Name], [First Name], [Product Title], [Completed Date]
	