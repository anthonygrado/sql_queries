DECLARE @UserFirstName nvarchar(50);
DECLARE @UserLastName nvarchar(50);
DECLARE @SearchFirstName nvarchar(50);
DECLARE @SearchLastName nvarchar(50);
DECLARE @SearchOrganization nvarchar(50);

SET @UserFirstName = 'Amanda'
SET @UserLastName = 'Silver'
SET @SearchOrganization = '5495011B-5BA1-40F5-95FB-6EE8B00FC3A5'
SET @SearchFirstName = '%'+@UserFirstName+'%'
SET @SearchLastName = '%'+@UserLastName+'%'

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT CT.[id]
      ,ORG.[OrganizationName] AS [Organization Name]
      ,LTRIM(RTRIM(LEFT(CT.[learnerFullname], charindex(',', CT.[learnerFullname])-1))) AS [Last Name]
      ,LTRIM(RTRIM(RIGHT(CT.[learnerFullname], len(CT.[learnerFullname])-charindex(',', CT.[learnerFullname])))) AS [First Name]
      ,CT.[usernameId] AS [LMS ID]      
      ,CT.[productTitle] AS [Product Title]
      ,CT.[accreditingBodyId] AS [accrediting Body Id]
      ,CT.[credits] AS [Credits]
      ,CT.[learnerLicense] AS [Learner License]
      ,CT.[completionDateUTC] AS [Completion Date]
      ,CT.[accreditingBodyExternalId] AS [Accrediting Body External Id]
      ,CT.[Learner_ID] AS [Learner ID]
  FROM [Learning_Products].[dbo].[CertificateTransactions] CT
  INNER JOIN [Learning_Products].[dbo].[Organizations] ORG
      ON CT.[organizationId] = ORG.[PMTID] 
  WHERE CT.[learnerFullname] LIKE '%'+@UserLastName+', '+@UserFirstName+'%'
  AND CT.[organizationId] = @SearchOrganization