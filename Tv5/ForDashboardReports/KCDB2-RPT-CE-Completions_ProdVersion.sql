/****** Script for Select CNE Completions By Product Version - KCDB2  ******/
DECLARE @SELECTED_TITLE nvarchar(36);
DECLARE @STARTDATE nvarchar(10);
DECLARE @ENDDATE nvarchar(10);
DECLARE @IGNORE_ORGS nvarchar (16);

/*Remove Results from Test Hospital Organization*/
SET @IGNORE_ORGS = '35'

/*Value for All or a Specific Product Title*/
SET @SELECTED_TITLE = ''

/*Date Range of Results*/
SET @STARTDATE = '01/01/2016'
SET @ENDDATE = '12/31/2016'


SELECT CT.[productTitle] AS [PRODUCT TITLE]
	,PRO.[VersionNumber] AS [VERSION NUMBER]
	,COUNT(CT.[id]) AS [COMPLETIONS]
	,CT.[credits] AS [UNITS AVAILABLE]
	,SUM(CT.[credits]) AS [CREDITS]      
FROM [Learning_Products].[dbo].[CertificateTransactions] CT
INNER JOIN [Learning_Products].[dbo].[Organizations] ORG
	ON ORG.[PMTID] = CT.[organizationId]
INNER JOIN [Learning_Products].[dbo].Products PRO
	ON PRO.[ItemID] = CT.[productId]  
WHERE CT.[accreditingBodyId] LIKE '%cne%'
	AND CT.[lastUpdatedUTC] >= @STARTDATE
    AND CT.[lastUpdatedUTC] <= @ENDDATE
    /*-- Test Hospital  -- */
		AND ORG.[Organization_ID] NOT IN (''+@IGNORE_ORGS+'')
	/*-- PLS Admins --*/
		AND CT.[learnerFullname] NOT IN ('Grado, Anthony', 'Anthony Grado', 'Grado,  Anthony')
GROUP BY CT.[productTitle], PRO.[VersionNumber], CT.[credits]