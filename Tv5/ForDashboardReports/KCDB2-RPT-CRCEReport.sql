/****** Script for Select CNE Completions By Product Version - KCDB2  ******/
DECLARE @SELECTED_TITLE nvarchar(36);
DECLARE @STARTDATE nvarchar(10);
DECLARE @ENDDATE nvarchar(10);
DECLARE @IGNORE_ORGS nvarchar (16);
DECLARE @CEType nvarchar(10);

/*Remove Results from Test Hospital Organization*/
--SET @IGNORE_ORGS = '35'

/*Value for All or a Specific CE Type*/
SET @CEType = 'crce'

/*Date Range of Results*/
SET @STARTDATE = '01/01/2017'
SET @ENDDATE = '03/31/2017'


SELECT DISTINCT CT.[productTitle] AS [Product Title]
	,CT.[accreditingBodyExternalId] AS [Accrediting Body External Id]
	,CT.[learnerLicense] AS [AARC Member Number]
	,LTRIM(RTRIM(LEFT(CT.[learnerFullname], charindex(',', CT.[learnerFullname])-1))) AS [Last Name]
	,LTRIM(RTRIM(RIGHT(CT.[learnerFullname], len(CT.[learnerFullname])-charindex(',', CT.[learnerFullname])))) AS [First Name]
	,CT.[learnerFullname] AS [RAW LearnerName]
	,DS.[abbr] AS [State of Residence]	
	,CONVERT(varchar(16),CT.[completionDateUTC],101) AS [Date of Completion]
FROM [Learning_Products].[dbo].[CertificateTransactions] CT
INNER JOIN [Learning_Products].[dbo].[Organizations] ORG
	ON ORG.[PMTID] = CT.[organizationId]
INNER JOIN [Learning_Products].[dbo].[def_states] DS
	ON ORG.[State_ID] = DS.[id]
WHERE CT.[accreditingBodyId] LIKE '%'+@CEType+'%'
	AND CT.[lastUpdatedUTC] >= @STARTDATE
    AND CT.[lastUpdatedUTC] <= @ENDDATE
    /*-- Test Hospital  -- */
		AND ORG.[Organization_ID] NOT IN ('35', '43')
	/*-- PLS Admins --*/
		AND CT.[learnerFullname] NOT IN ('Grado, Anthony', 'Anthony Grado', 'Grado,  Anthony')
ORDER BY [PRODUCT TITLE], [Last Name], [First Name]